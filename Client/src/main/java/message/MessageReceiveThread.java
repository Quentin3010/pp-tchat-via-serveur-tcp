package main.java.message;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import javafx.application.Platform;
import main.java.connection.ConnectionTCP;
import main.java.gui.App;

public class MessageReceiveThread extends Thread{

	@Override
	public void run() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(ConnectionTCP.getIn()));
		while (!ConnectionTCP.getSocket().isClosed()) {
			try {
				String msg = reader.readLine();
				System.out.println("=> " + msg);

				JSONObject obj = (JSONObject) JSONValue.parse(msg);
				String type = (String) obj.get("type");

				if(type.equals(MessageType.ONLINE_LIST.getValue())) {
					Platform.runLater(() -> {
						String[] listePseudo = ((String) obj.get("listePseudo")).split(",");
						String[] listeUUID = ((String) obj.get("listeUUID")).split(",");
						for(int i = 0; i<listePseudo.length; i++) {
							App.addName(listeUUID[i], listePseudo[i]);
						}
					});
				}else {
					String pseudo = ((String) obj.get("pseudo"));
					String uuid = (String) obj.get("uuid");

					Platform.runLater(() -> {
						if (type.equals(MessageType.SENDING_MESSAGE.value)) {
							App.text.setText(App.text.getText() + pseudo + " > " + ((String) obj.get("msg")) + "\n");
						} else if (type.equals(MessageType.JOINING.value)) {
							App.text.setText(App.text.getText() + "=> " + pseudo + " a rejoint le channel\n");
							App.addName(uuid, pseudo);
						} else if (type.equals(MessageType.LEAVING.value)) {
							App.text.setText(App.text.getText() + "=> " + pseudo + " a quitt� le channel\n");
							App.removeName(uuid);
						}
						App.tchatBox.setVvalue(1.0);
					});
				}
			} catch (Exception e) {}
		}
	}

}
