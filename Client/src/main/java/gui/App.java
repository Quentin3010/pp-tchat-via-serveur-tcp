package main.java.gui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.java.connection.ConnectionTCP;
import main.java.message.Message;
import main.java.message.MessageReceiveThread;

public class App extends Application {
	public static final MessageReceiveThread thread = new MessageReceiveThread();
	public static final Text text = new Text("");
	
	public static final VBox participantList = new VBox();
	public static final Map<String, Text> mapOnline = new HashMap<String, Text>();
	
	public static ScrollPane tchatBox;
	
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Tchat en ligne");
        HBox root = new HBox();
        root = addLeftSection(root);
        root = addRightSection(root);
        
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.setResizable(false);
        
        primaryStage.show();
        
        
        //FENETRE DE LOGIN
        new LoginWindows(primaryStage);
    }
    
    private HBox addLeftSection(HBox hbox) {
    	VBox vbox = new VBox();
    	vbox = addMessageArea(vbox);
    	vbox = addWritingArea(vbox);
    	
    	vbox.setPrefWidth(400);
    	vbox.setPrefHeight(400);
    	hbox.getChildren().add(vbox);
    	return hbox;
    }
    
    private VBox addMessageArea(VBox vbox) {
    	Text title = new Text("Messages recu :");
    	title.prefHeight(50);
    	
    	tchatBox = new ScrollPane(text);
    	
        tchatBox.setPrefViewportHeight(200);
        tchatBox.setFitToWidth(true);
        tchatBox.setFitToHeight(true);
        
        vbox.getChildren().addAll(title, tchatBox);
        return vbox;
    }
    
    private VBox addWritingArea(VBox vbox) {
    	Text title = new Text("Message a envoyer :");
    	title.prefHeight(50);
    	
    	HBox hbox = new HBox();
    	
    	TextArea textArea = new TextArea();
    	textArea.prefHeight(100);
        textArea.setWrapText(true);

        ScrollPane scrollPane = new ScrollPane(textArea);
        scrollPane.setPrefViewportHeight(100);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        
        Button sendButton = new Button("Send");
        sendButton.setOnAction(e -> {
        	if(!textArea.getText().isEmpty()) {
        		try {
    				ConnectionTCP.getOut().write(Message.createSendingMessage(ConnectionTCP.getUserData(), textArea.getText()).getBytes());
    				textArea.setText("");
        		} catch (IOException e1) {}
        	}
        });

        hbox.getChildren().addAll(scrollPane, sendButton);
        
        vbox.getChildren().addAll(title, hbox);
        return vbox;
    }

    private HBox addRightSection(HBox hbox) {
    	VBox vbox = new VBox();
    	vbox = addParticipantsList(vbox);
    	
    	vbox.setPrefWidth(200);
    	vbox.setPrefHeight(400);
    	hbox.getChildren().add(vbox);
    	return hbox;
    }
    
    private VBox addParticipantsList(VBox vbox) {
    	Text title = new Text("Participants");
    	title.prefHeight(50);
    	
    	participantList.prefHeight(350);
    	
        ScrollPane scrollPane = new ScrollPane(participantList);
        scrollPane.setPrefViewportHeight(350);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        
        //Permet d'�tre en bas de la scroll bar
        scrollPane.setVvalue(0);
    	
        vbox.getChildren().addAll(title, scrollPane);
    	return vbox;
    }
    
    @SuppressWarnings("deprecation")
	@Override
    public void stop() throws Exception {
    	if(thread.isAlive()) {
    		thread.stop();
    	}
    	ConnectionTCP.closeConnection();
    	super.stop();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    public static void addName(String uuid, String pseudo) {
    	Text text = new Text(pseudo);
    	if(!mapOnline.containsKey(uuid)) {
    		participantList.getChildren().add(text);
        	mapOnline.put(uuid, text);
    	}
    }
    
    public static void removeName(String uuid) {
    	Text text = mapOnline.get(uuid);
    	mapOnline.remove(uuid);
    	participantList.getChildren().remove(text);
    	
    }
}