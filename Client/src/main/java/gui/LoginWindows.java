package main.java.gui;

import java.io.IOException;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.java.connection.ConnectionTCP;
import main.java.message.Message;

public class LoginWindows extends Stage  {
	public final Stage parent;
	
	TextField pseudoField = new TextField("JeSuisMister");
	TextField addrField = new TextField("193.70.40.178");
	TextField portField = new TextField("5000");
	
    public LoginWindows(Stage parent) {
    	this.parent = parent;
        
        VBox root = new VBox();
        root = addLabelAndTextField(root);
        root = addLoginBuuton(root);
        setCloseEvent();
        
        this.setTitle("Connection au tchat");
        this.setScene(new Scene(root, 300, 200));
        this.initModality(Modality.APPLICATION_MODAL);
        
        this.setResizable(false);
        
        this.show();
    }
    
    public VBox addLabelAndTextField(VBox vbox) {
    	Label pseudoLabel = new Label("Pseudo : ");
        Label addrLabel = new Label("Adresse du serveur : ");     
        Label portLabel = new Label("Port du serveur : ");
    	
        vbox.getChildren().addAll(pseudoLabel, pseudoField, new Separator(), addrLabel, addrField, new Separator(), portLabel, portField, new Separator());
    	
    	return vbox;
    }
    
    public VBox addLoginBuuton(VBox vbox) {
    	Button loginButton = new Button("Login");
        loginButton.setOnAction(e -> {
        	if(ConnectionTCP.createConnection(pseudoField.getText(), addrField.getText(), portField.getText())) {
                App.thread.start();
                
                try {
                	ConnectionTCP.getOut().write(Message.createJoinMessage(ConnectionTCP.getUserData()).getBytes());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
                
        		this.hide();
        	}else {
        		addrField.setText("");
        		portField.setText("");
        	}
		});
        
        vbox.getChildren().add(loginButton);
        
        return vbox;
    }
    
    public void setCloseEvent() {
    	setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
            	if(event.getEventType().equals(WindowEvent.WINDOW_CLOSE_REQUEST)) parent.close();
            }
        });
    }
    
    
}