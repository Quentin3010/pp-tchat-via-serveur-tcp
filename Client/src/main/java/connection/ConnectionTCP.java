package main.java.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConnectionTCP {
	private static Socket socket;
	private static ConnectionData userData;
	private static OutputStream out;
	private static InputStream in;
	
    private ConnectionTCP() {
    }
    
    /**
     * Fonction pour creer une connection a partir d'un IP et d'un port
     * @param serverAddress
     * @param serverPort
     * @return
     */
	public static boolean createConnection(String pseudo, String addr, String port) {
		try {
			ConnectionTCP.closeConnection();
			userData = new ConnectionData(pseudo, addr, Integer.parseInt(port));
            socket = new Socket(addr, Integer.parseInt(port));
            out = socket.getOutputStream();
            in = socket.getInputStream();
            return true;
        } catch (UnknownHostException e) {
            System.err.println("Adresse du serveur inconnue : " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Erreur d'E/S lors de la connexion au serveur : " + e.getMessage());
        } catch (NumberFormatException e) {
        	System.err.println("Le port fournis n'est pas au format num�rique : " + e.getMessage());
        }
		return false;
	}
	
	public static void closeConnection() {
		try {
			if(socket!=null) {
				in.close();
				out.close();
				socket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Socket getSocket() {
		return socket;
	}

	public static ConnectionData getUserData() {
		return userData;
	}

	public static OutputStream getOut() {
		return out;
	}

	public static void setOut(OutputStream out) {
		ConnectionTCP.out = out;
	}

	public static InputStream getIn() {
		return in;
	}

	public static void setIn(InputStream in) {
		ConnectionTCP.in = in;
	}
	
	
}
