# Tchat TCP

Quentin BERNARD

## Description du projet

Le projet "Tchat TCP" a été conçu dans le cadre de mon exploration approfondie du protocole de communication TCP pendant mon semestre 5 de la Licence Informatique en troisième année. Arborant un nom parodique, ce projet vise à offrir une expérience immersive dans le monde de la communication réseau. 

Il se compose de deux programmes distincts : l'un déclenche une application graphique côté client, tandis que l'autre configure un serveur agissant en tant que point de diffusion (multicast), responsable de la réception et de la redistribution des messages d'un utilisateur à tous les autres. 

Bien que le projet ne prétende pas à la grandeur, il représente une opportunité ludique d'élaborer une interface graphique en JavaFX à partir de zéro et d'explorer les subtilités du protocole TCP.

## Fonctionnalités

- Mise en place d'un serveur.
- Côté client :
    - Connexion avec un pseudonyme à un serveur spécifié par son adresse IP et son port.
    - Envoi de messages une fois la connexion établie.
    - Affichage dynamique de la liste des utilisateurs actuellement connectés au tchat.

## Mon rôle

Mon implication dans le projet a été complète, allant de la planification à la conception. J'ai pris en charge la conception intégrale du projet, assurant ainsi la cohérence et la réussite de chaque étape.

