package message;

public enum MessageType {
	ONLINE_LIST("ONLINE_LIST"),
    SENDING_MESSAGE("SENDING_MESSAGE"),
    JOINING("JOINING"),
    LEAVING("LEAVING");

    public final String value;

    MessageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
