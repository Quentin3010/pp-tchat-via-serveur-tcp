package message;

import org.json.simple.JSONObject;

import connection.ConnectionData;
import server.UserData;

public class Message {
	public final MessageType type;
	public final String pseudo;
	public final String uuid;
	public String msg;
	
	public Message(MessageType type, ConnectionData data) {
		this(type, data, null);
	}
	
	public Message(MessageType type, ConnectionData data, String msg) {
		this.type = type;
		this.pseudo =  data.getUsername();
		this.uuid = data.getUuid();
		this.msg = msg;
	}

	@SuppressWarnings("unchecked")
	public static String createSendingMessage(ConnectionData data, String msg) {
		JSONObject obj = new JSONObject();    
		obj.put("type", MessageType.SENDING_MESSAGE.getValue());
		obj.put("msg", msg);
		obj.put("pseudo", data.getUsername());
		obj.put("uuid", data.getUuid());
		return obj.toJSONString() + "\n";
	}

	@SuppressWarnings("unchecked")
	public static String createJoinMessage(ConnectionData data) {
		JSONObject obj = new JSONObject();    
		obj.put("type", MessageType.JOINING.getValue());
		obj.put("pseudo", data.getUsername());
		obj.put("uuid", data.getUuid());
		return obj.toJSONString() + "\n";
	}
	
	@SuppressWarnings("unchecked")
	public static String createLeaveMessage(UserData userData) {
		JSONObject obj = new JSONObject();    
		obj.put("type", MessageType.LEAVING.getValue());
		obj.put("pseudo", userData.pseudo);
		obj.put("uuid", userData.uuid);
		return obj.toJSONString() + "\n";
	}
	
	@SuppressWarnings("unchecked")
	public static String createLeaveMessage(String pseudo, String uuid) {
		JSONObject obj = new JSONObject();    
		obj.put("type", MessageType.LEAVING.getValue());
		obj.put("pseudo", pseudo);
		obj.put("uuid", uuid);
		return obj.toJSONString() + "\n";
	}
}
