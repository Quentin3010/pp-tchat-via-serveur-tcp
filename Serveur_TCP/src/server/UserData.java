package server;

public class UserData {
	public final String pseudo;
	public final String uuid;
	
	public UserData(String pseudo, String uuid) {
		this.pseudo = pseudo;
		this.uuid = uuid;
	}
}
