package server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import message.MessageType;

public class ServerTCP {
public static Map<UserData, OutputStream> listeClients = new HashMap<UserData, OutputStream>();
    
    public static void main(String[] args) {
        
        try (ServerSocket server = new ServerSocket(5000)) {
            String ip = "IP_MACHINE:" + server.getLocalPort();
            System.out.println("=========== Ouverture du serveur " + ip + " ===========");
            
            while(true) {
                Socket socket = server.accept();
                TCPClient client = new TCPClient(socket);
                client.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /*
     * Envoie un message a tous les clients connecte (listeClients), sauf a l'expediteur
     */
    public static void sendMessageAll(String message) throws IOException {
    	if(message.isBlank()) return;
    	
    	System.out.println("=> " + message);
        for(OutputStream output : listeClients.values()) {
        	output.write((message + "\n").getBytes());	
        }
    }
    
    @SuppressWarnings("unchecked")
	public static void sendOnlineList(OutputStream output) throws IOException {
    	if(listeClients.isEmpty()) return;
    	
    	String listePseudo = "";
    	String listeUUID = "";
    	for(UserData d : listeClients.keySet()) {
    		listePseudo += d.pseudo + ",";
    		listeUUID += d.uuid + ",";
    	}
    	listePseudo.substring(0, listePseudo.length()-1);
    	listeUUID.substring(0, listeUUID.length()-1);
    	
    	JSONObject obj = new JSONObject();
    	obj.put("type", MessageType.ONLINE_LIST.getValue());
    	obj.put("listePseudo", listePseudo);
    	obj.put("listeUUID", listeUUID);
    	output.write((obj.toJSONString() + "\n").getBytes());
    }
}
