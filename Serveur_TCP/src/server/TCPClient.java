package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import message.Message;

public class TCPClient extends Thread{
	private final Socket socket;
	private final InputStream input;
	private final OutputStream output;
	private UserData userData;

	public TCPClient(Socket socket) throws IOException {
		this.socket = socket;
		this.input = socket.getInputStream();
		this.output = socket.getOutputStream();
		
		ServerTCP.sendOnlineList(output);
	}

	@Override
	public void run() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String msg;
		try {
			msg = reader.readLine();
			JSONObject obj = (JSONObject) JSONValue.parse(msg);
			
			this.userData = new UserData((String) obj.get("pseudo"), (String) obj.get("uuid"));
			ServerTCP.listeClients.put(this.userData, this.socket.getOutputStream());
			ServerTCP.sendMessageAll(msg);

			msg = reader.readLine();
			while(msg!=null) {
				ServerTCP.sendMessageAll(msg);
				msg = reader.readLine();
			}
			
			input.close();
			socket.close();

			ServerTCP.listeClients.remove(this.userData);
			output.close();	
			
			ServerTCP.sendMessageAll(Message.createLeaveMessage(this.userData));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
