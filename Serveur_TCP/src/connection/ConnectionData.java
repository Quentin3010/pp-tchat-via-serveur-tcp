package connection;

import java.util.UUID;

public class ConnectionData {
	private final String uuid = UUID.randomUUID().toString();
	private final String username;
	private final String serverAddress;
	private final int serverPort;
	
	public ConnectionData(String username, String serverAddress, int serverPort) {
		this.username = username;
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
	}

	public String getUsername() {
		return username;
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public int getServerPort() {
		return serverPort;
	}

	public String getUuid() {
		return uuid;
	}
}
