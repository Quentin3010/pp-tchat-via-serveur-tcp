#!/bin/bash

# Compilation avec le chemin de classe incluant le jar
javac -cp .:json-simple-1.1.1.jar src/message/*.java src/connection/*.java src/server/*.java

# Exécution du programme avec le chemin de classe incluant le jar et le nom complet de la classe
java -cp .:json-simple-1.1.1.jar:src server.ServerTCP

